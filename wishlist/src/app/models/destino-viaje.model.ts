import { v4 as uuid } from 'uuid';

export class DestinoViaje {
  selected: boolean;
  public servicios: string[];
  public id = uuid();
  public votes = 0;


  constructor(public nombre: string, public imagenUrl: string) {
    this.servicios = ['Pileta', 'Desayuno'];
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(s: boolean) {
    this.selected = s;
  }

  voteUP() {
    this.votes++;
  }

  voteDown() {
    this.votes--;
  }
}
