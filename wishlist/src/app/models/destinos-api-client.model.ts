import { Injectable, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import { Appstate, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoDestionAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

  constructor(
    private store: Store<Appstate>,
    @Inject(forwardRef( () => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
    ) {
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });
  }

  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre}, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestionAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la dbl');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
  }

  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  getById(id: string): DestinoViaje {
    // console.log('destino:' + this.destinos);
    return this.destinos.filter( (d) => {
      return d.id.toString() === id;
    }
    )[0];
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

}
