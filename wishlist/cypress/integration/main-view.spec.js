const { createYield } = require("typescript");

describe('ventana principal', () => {
  it('tiene encabezado correcto y en español por defecto', () => {
    cy.visit('http://localhost:4200');
    cy.contains('angular-whishlist');
    cy.get('h1 b').should('contain', 'HOLA es');
  });
});

describe('Escribir en textbox nombre', () => {
  it('.type() - escribir en DOM element', () => {
    cy.get('#nombre')
      .type('Rancagua').should('have.value', 'Rancagua')

      .type('{del}{selectall}{backspace}')

      .type('Machali', { delay: 1000 })
      .should('have.value', 'Machali')
  });
});

describe('Escribir en textbox url', () => {
  it('.type() - escribir en DOM element', () => {
    cy.get('#imagenUrl')
      .type('https://upload.wikimedia.org/wikipedia/commons/e/e4/Cuesta_del_obispo_01.jpg').should('have.value', 'https://upload.wikimedia.org/wikipedia/commons/e/e4/Cuesta_del_obispo_01.jpg')

  });
});

describe('Click en boton Guardar', () => {
  it('.click() - click on a DOM element', () => {
    // https://on.cypress.io/click
    cy.get('.btn-primary').click()

    cy.get('.ng-tns-c74-0').should('contain', 'Machali');

  });
});
